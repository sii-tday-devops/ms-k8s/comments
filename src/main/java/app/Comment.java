package app;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Comment extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Schema(format = "uuid", type = SchemaType.STRING,
            pattern = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}",
            readOnly = true, example = "4b502574-fa06-408d-98c9-5fc88bdc863b")
    public UUID uuid;

    @Size(max = 254, min = 5)
    @Column(length = 255, nullable = false)
    public String author;

    @Column(nullable = false, columnDefinition = "text")
    public String content;

    @Column(nullable = false, updatable = false)
    @Schema(format = "uuid", type = SchemaType.STRING,
            pattern = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}",
            example = "4b502574-fa06-408d-98c9-5fc88bdc863b")
    public UUID establishment;

    @Schema(readOnly = true)
    @Column(nullable = false)
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    public Date createdAt;

    @Schema(readOnly = true)
    @Column(nullable = false)
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    public Date updatedAt;

    public static Comment findByUUID(UUID uuid) {
        return find("uuid", uuid).firstResult();
    }
}
